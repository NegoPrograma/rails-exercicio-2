Rails.application.routes.draw do

  root 'static_pages#home'# setando a pagina inicial, nomedapasta#arquivohtml

  resources :users, except: [:new], path:"usuarios"
  #cria as rotas para os usuários, menos as que estiverem no except
  #a função resources cria 7 rotas referente ao usuário

  get '/cadastro', to: 'users#new'
  #a get pega a informação submitada no link cadastro para a função

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'

  delete '/logout', to: "sessions#destroy"
end
