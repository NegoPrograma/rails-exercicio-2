class User < ApplicationRecord
    has_secure_password
    validates :password,presence: true, length: { minimum: 6 } # validates presence diz que não pode estar vazio, length possui parametros que determinam o tamanho, sendo minimo ou máximo
    #Utilização da lib bcrypt
end
