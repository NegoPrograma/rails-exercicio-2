class StaticPagesController < ApplicationController
    def home
        render 'home'
    end

    private

    def barra_acesso_a_deslogados
        if !logged_in?
            redirect_to root_path
        end
    end

    def barra_acesso_a_logados
        if logged_in?
            redirect_to current_user
        end
    end
end
