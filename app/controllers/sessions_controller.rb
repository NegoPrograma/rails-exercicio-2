class SessionsController < ApplicationController
    before_action :barra_acesso_a_logados, only: [:new]
    before_action :barra_acesso_a_deslogados, only: [:destroy]
    def new

    end

    def create
        user = User.find_by(email: params[:session][:email])
        if user && user.authenticate(params[:session][:password])
            log_in(user)
            redirect_to user
        else
            render 'new'
        end
    end

    def destroy
        log_out
        redirect_to root_path
    end

    def barra_acesso_a_logados
        if logged_in?
            redirect_to current_user
        end
    end

    def barra_acesso_a_deslogados
        if !logged_in?
            redirect_to root_path
        end
    end



end
